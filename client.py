import socket
import sys


BUFFER_SIZE = 1024
ENCODING = 'utf-8'


class Client:

    def __init__(self, host, port):
        self.sock = socket.socket()
        self.sock.connect((host, port))

    def send(self):
        while True:
            data = input('Enter your message (empty for exiting): ')
            if not data or data.isspace():
                self.stop('Exiting...')
                break

            self.sock.send(data.encode(ENCODING))
            data = self.sock.recv(BUFFER_SIZE)
            if not data:
                self.stop('Server is not available.')
                break

            print(data.decode(ENCODING))

    def stop(self, message):
        print(message)
        self.sock.close()


def main(host, port):
    client = Client(host, port)
    client.send()


if __name__ == '__main__':
    try:
        host, port = sys.argv[1], int(sys.argv[2])
    except IndexError:
        host, port = 'localhost', 9090

    main(host, port)
