from pony import orm


db = orm.Database()


class Message(db.Entity):
    message = orm.Required(str)


def bind_db():
    db.bind(provider='sqlite', filename='database.sqlite', create_db=True)
    db.generate_mapping(create_tables=True)
