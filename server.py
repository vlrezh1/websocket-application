import socket
import sys

import database


BUFFER_SIZE = 1024
ENCODING = 'utf-8'
database.bind_db()


class Server:

    def __init__(self, host, port):
        self.sock = socket.socket()
        self.sock.bind((host, port))
        self.sock.listen(1)

    def listen(self):
        while True:
            print('Waiting for client...')
            try:
                self.client, _ = self.sock.accept()
            except KeyboardInterrupt:
                self.stop_server('Server is shutting down...')
                break
            print('Client connected.')

            while True:
                try:
                    data = self.client.recv(BUFFER_SIZE)
                except KeyboardInterrupt:
                    self.stop_client('Disconnecting from client...')
                    break

                if not data:
                    self.stop_client('Client disconnected.')
                    break

                data = data.decode(ENCODING)
                if data == 'LIST':
                    data = self.show_list()
                else:
                    self.add_to_db(data)
                    data = self.sort_words(data)
                self.client.send(data.encode(ENCODING))

    def stop_server(self, message):
        print(message)
        self.sock.close()

    def stop_client(self, message):
        print(message)
        self.client.close()

    @staticmethod
    def add_to_db(data):
        with database.orm.db_session:
            database.Message(message=data)

    @staticmethod
    def show_list():
        with database.orm.db_session:
            data = list(database.orm.select(M.message\
                for M in database.Message))
            data = '\n'.join(data)

        return data

    @staticmethod
    def sort_words(data):
        words = list(set(data.split()))
        words.sort()
        data = '\n'.join(words)

        return data


def main(host, port):
    server = Server(host, port)
    server.listen()


if __name__ == '__main__':
    try:
        host, port = sys.argv[1], int(sys.argv[2])
    except IndexError:
        host, port = '', 9090

    main(host, port)
